# Blocage de pubs et traqueurs avec Unbound

Ce script `bash` a pour but de transformer le résolveur DNS [Unbound](https://nlnetlabs.nl/projects/unbound/about/) en résolveur menteur (ou « politique » dans la terminologie du [RFC 8499](https://www.rfc-editor.org/rfc/rfc8499.txt)) et bloquer les principaux domaines servant de la publicité ou des traqueurs.

La technique classique – et la plus simple – pour bloquer des domaines est le fichier [`hosts`](https://fr.wikipedia.org/wiki/Hosts) sde la machine : présent dans tous les OS modernes (même Windows), facile à mettre en place... L'avantage de passer plutôt par un résolveur menteur pour bloquer ces domaines indésirables est :
1. de servir potentiellement à tout un réseau local
2. de ne pas laisser fuiter de requêtes pour ces domaines vers l'extérieur
3. de bien bloquer les enregistrements `A` (IPv4), `AAAA` (IPv6), `TXT` (et tout le reste), un fichier `hosts` ne bloquant en général qu'IPv4

Attention ceci dit, utiliser ce genre de blocage via le DNS ne protège pas de tout et **il est donc indispensable de continuer à utiliser les modules habituels de vos navigateurs (uBlock Origin, uMatrix,...)**.

Il est pensé pour s'exécuter sous Debian & dérivés. Le tester sous d'autres distributions (Arch & cie) est dans la liste des choses à faire.

Le principe est relativement simple : le script télécharge des listes connues de domaines (par défaut celles d'[AdAway](https://f-droid.org/packages/org.adaway/)) et construit un fichier de configuration pour Unbound. Les requêtes pour les domaines concernés se verront répondre `NXDOMAIN`.

- [Pré-requis](#pré-requis)
- [Installation automatique](#installation-automatique)
- [Installation manuelle](#installation-manuelle)
	- [Service systemd](systemd)
	- [NetworkManager et Wi-Fi](#networkmanager-et-wi-fi)
- [Fonctionnement](#fonctionnement)
	- [Listes de domaines à récupérer](#listes-de-domaines-à-récupérer)
	- [Description sommaire](#description-sommaire)
	- [Fichiers](#fichiers)
	- [Test](#test)
	- [Limitations](#limitations)
	- [Garde-fous](#garde-fous)
- [Pourquoi pas Pi-Hole ?](#pourquoi-pas-pi-hole-)
- [TODO](#todo)
- [Licence](#licence)


## Pré-requis

3 logiciels sont nécessaires au bon fonctionnement du script :
- Unbound (bien sûr). N'importe quelle version pas trop vielle doit faire l'affaire (disons à partir de la 1.5, mais ça doit passer avec des versions plus anciennes)
- `jq` pour gérer le JSON contenant les informations sur les listes à télécharger
- `curl` pour récupérer les listes (il peut être remplacé par `wget`, la variante est présente en commentaire dans le script)

**Remarque :** `jq` et `curl` ne sont pas installés par défaut sous Debian.

## Installation automatique

**Note :** Si vous aviez fait une installation manuelle et que vous souhaitez désormais passer par le
l'installation via paquet, il faut au préalable supprimer tous les fichiers (scripts, fichier de service...)

Pour les utilisateur·trice·s de Debian/Ubuntu et dérivés, un paquet .deb est disponible [depuis mon site Web](https://www.shaftinc.fr/misc/packages/unbound-adblock/). Une fois la dernière version téléchargée, l'installation se fait simplement :

```
$ sudo dpkg -i unbound-adblock_<version>_all.deb
```

Si une nouvelle version est disponible, il suffit de télécharger le nouveau paquet et de relancer cette commande.

Pour supprimer le script, ainsi que l'entièreté de ces fichiers (configuration Unbound générée, listes téléchargées...), on utilise :

```
$ sudo dpkg -P unbound-adblock
```

La suppression via l'option `-r` de `dpkg` (ou l'équivalent avec `apt`) va laisser la plupart des fichiers en place, notamment la configuration générée pour Unbound.

## Installation manuelle

À faire en tant que `root` :
1. S'assurer que `root` est propriétaire de tous les fichiers, changer le propriétaire au besoin
2. Copier le [script](unbound-adblock) dans `/usr/local/bin` (ou ailleurs)
3. Le rendre éxécutable via la commande `chmod +x /usr/local/bin/unbound-adblock`
4. Copier [liste-adblock.json](liste-adblock.json) dans `/var/lib/unbound`
5. Copier [adblock](adblock) dans `/etc/logrotate.d/` (**Attention :** ne pas supprimer l'option `delaycompress` au risque de casser la détection de retrait de liste du JSON).
6. Exécuter le script via `# /chemin/vers/unbound-adblock /chemin/de/liste.json`

### Service systemd

Un moyen efficace, ayant ma préférence, de lancer le script est de le faire au démarrage de la machine, ainsi une relance d'Unbound est moins pénalisante étant donné que redémarrer ce résolveur vide son cache. Pour ce faire, le plus simple est d'en faire un service `systemd` :
1. copier [adblock.service](adblock.service) dans `/etc/system.d/system`
2. activer et démarrer le service via `systemctl enable --now adblock.service`
3. vérifier que tout c'est bien passé soit via le status du service, soit via le fichier de log

### NetworkManager et Wi-Fi

Sur les systèmes utilisant NetworkManager pour gérer le réseau (notamment les environnements de bureau Gnome, KDE,...) et se connectant via Wi-Fi, l'éxécution du script au démarrage échoue par défaut. En effet, pour les connexions nécéssitant un secret, [NetworkManager attend que l'utilisateur·trice possédant ce secret ouvre une session pour se connecter](https://wiki.archlinux.org/index.php/NetworkManager#Connect_to_network_with_secret_on_boot) (le secret en question étant par défaut chiffré). Pour éviter cet éceuil, sans que ce soit à la personne utilsant le script de contourner le problème (en rendant accessible le réseau Wi-Fi auquel se connecter à tous les comptes de la machine par exemple), il est possible d'installer un *timer* `systemd` optionnel. Pour ce faire :
1. copier [adblock.timer](adblock.timer) dans `/etc/system.d/system`
2. activer et le démarrer le timer via `systemctl enable --now adblock.timer`.

Ce *timer* va tenter de relancer le script toutes les minutes si le service est planté. Voir #3 pour un descriptif plus précis du problème. [Ce billet décrit la mécanique](https://www.shaftinc.fr/automatisation-taches-systemd.html). [Le ticket #3](#3) discute d'autres options possibles pour résoudre ce problème.

Une autre solution pour les machines dans ce cas est de ne pas faire tourner unbound-adblock en tant que service `systemd` mais le déclencher, plus classiquement, via un `cron`. Auquel cas, il est possible de déplacer le script (ou de l'y faire pointer via un lien symbolique) dans `/etc/cron.weekly` voir `/etc/cron.monthly` pour une exécution hebdomaire ou mensuelle (la pratique montre que les 4 listes fournies par défaut ne change qu'à la marge avec le temps).

## Fonctionnement

### Listes de domaines à récupérer

Les listes à utiliser peuvent être soit :
- un bête listing de domaine (1 domaine/ligne)
- un fichier `hosts`

Elles stockés dans un JSON faisant office de tableau :

```json
[
	{
		"Nom": "Super liste 1"
		"URL": "https://adblock.example/hosts.txt"
		"Hash": "b875f928546aee7855cb1db9afc8ab3f1a8a34d43de5bbd62f7076d7ba9f3917"
	},
	...
	{
		"Nom": "Super liste n"
		"URL": "https://www.example.org/list/adblock.txt"
		"Hash": null
	}
]
```

Lors de l'ajout d'une nouvelle liste, ou lors de la première utilisation du script, le hash doit valoir `null` (sans guillemets). Il est en réalité possible de mettre n'importe quoi (sauf une chaîne vide), mais `null` permet de logguer correctement les actions réalisées.

### Description sommaire

À chaque éxécution, le script télécharge les listes paramétrée dans le JSON, puis leur `sha256` est calculé et comparé à la valeur qui y est stockée. S'il est identique, on ne fait rien, sinon la liste est nettoyée (suppression des commmentaires, des lignes contenant l'adresse du `localhost`,...), sauvegardée, et ajoutée à la liste de domaines globale. Cette dernière est ensuite triée et dédoublonnée, le fichier de configuration d'Unbound est finalement généré et Unbound est rechargée. Une copie de sauvegarde du précédent fichier de configuration est fait et un `diff` est fait entre la nouvelle et l'ancienne liste globale.

Le blocage dans Unbound se fait de la sorte :

```
...
local-zone: "publicite.example" static
local-zone: "mouchard.domaine.example" static
...
```

Voir [le billet de blog que j'avais écrit](https://www.shaftinc.fr/blocage-pubs-unbound.html) lors de la publication de la toute première version de ce script, pour une discussion sur le choix du type `static`. Il s'avère assez peu gourmand comparativement à d'autres possibilités disponible dans Unbound (Il consomme entre 40 et 50 Mio de RAM pour ≈55 000 domaines bloqués, ce qui représente certes une certaine augmentation – il doit consommer une vingtaine de Mio sinon après démarrage – mais qui ce reste tout à fait correct sur un système moderne, y compris sur une petite machine – c'est relativement indolore sur un Raspberry Pi 2 et son gigaoctet de RAM).

### Fichiers

Par défaut, le script va écrire dans ces fichiers :
- `/etc/unbound/unbound.conf.d/adblock.conf` : le fichier de configuration d'Unbound généré en bout de course. Ce chemin convient aux systèmes Debian & dérivés, mais pas pour Arch & consorts
- `/var/lib/unbound/liste-adblock.json` : Le JSON passé en paramètre à l'éxécution
- `/var/lib/unbound/liste-ad.txt` : la liste globale des domaines bloqués (nettoyée et dédoublonnée)
- `/var/lib/unbound/modif-ad.diff` : le `diff` avec la précédente liste
- `/var/lib/unbound/adblock.conf.bak` : la sauvegarde du précédent fichier de configuration
- `/var/lib/unbound/nom_de_la_liste.list` : sauvegardes nettoyées des listes téléchargées, afin de construire la liste finale de blocage (voir #5)
- `/var/log/adblock.log` : le log. Pensez à la rotation des logs (cf. [Installation](#installation)). Par ailleurs, le script écrit aussi des informartions plus synthétiques dans le `syslog` (utile quand le script est utilisé en tant que service)

### Test

Afin de tester que le comportement d'Unbound est bien correct après l'exécutuion du script, on peut prendre un domaine dans liste de domaines bloqués (google-analytics.com par exemple) :
```
$ dig @::1 google-analytics.com

; <<>> DiG 9.11.5-P4-5.1+b1-Debian <<>> google-analytics.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 5030
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
...
```

Les informations importantes sont :
1. La réponse **NXDOMAIN** bien sûr, indiquant que le domaine n'existe pas
2. Le bit **AA** signifiant « Authoritative Answer », la réponse du serveur intérrogé (le `localhost` ici en l'occurence) fait autorité. Aucun autre serveur n'a donc été intérrogé

### Limitations

- A priori, risque de ne pas fonctionner si Unbound est « chrooté ». (Il ne l'est pas par défaut sous Debian & dérivés)

### Garde-fous

La résolution DNS étant un service essentiel, plusieurs garde-fous sont en place afin de ne pas avoir un résolveur planté ou redémarré pour rien. Le script s'arrête donc si :
- Le téléchargement d'au moins la moitié des listes de blocages a échoué, afin de ne pas se retrouver avec un nombre de domaines bloqués nul ou trop faible. En dessous (typiquement 1 téléchargement de liste sur 4 qui échoue), la sauvegarde (`nom_de_la_liste.list`) de la liste dont la récupération a échoué est utilisée : au pire, cette liste a subi une mise à jour sera rattrappée au prochain déclenchement du script. Si c'est le téléchargement d'une liste tout juste ajoutée au JSON qui échoue, il n'y a pas d'autres solutions, pour l'instant, que de relancer le script
- Si le fichier JSON n'est pas correct
- Si la vérification de la configuration d'Unbound (via `unbound-checkconf`) échoue

## Pourquoi pas Pi-Hole ?

J'ai [râlé dans le passé sur Pi-Hole](https://www.shaftinc.fr/pi-hole-blocage-pubs.html). Il est désormais possible d'[utiliser Pi-Hole conjointement avec Unbound](https://docs.pi-hole.net/guides/unbound/), ce qui était un des reproches que je lui faisais. Mais j'en ai d'autres :
- Le fait – et c'est mis en avant sur la page du projet – de par défaut logguer absolument toutes les requêtes, ce qui est un moyen d'espionnage bien trop puissant. Il est possible de désactiver la chose, mais tout de même.
- L'ensemble me semble un peu lourd (aka « c'est mignon les beaux graphique mais je m'en fiche un peu quand même ») : en plus de `dnsmasq`, le résolveur minimum – et serveur DHCP, Pi-Hole installe un serveur Web (`lighttpd`), `php` et une base `sqlite`
- Si j'avais utilisé Pi-Hole, je n'aurai rien appris :)

## TODO

- [ ] Regarder quelles listes utilise [Blokada](https://blokada.org) (voir #2)
- [x] ~~Trouver une solution au problème Wi-Fi+Network Manager (voir #3)~~
- [ ] Prendre en compte les distributions autres que Debian & dérivés (et n'utilisant pas `conf.d`)
- [ ] Améliorer la gestion des fichiers temporaires
- [ ] Tester avec Unbound chrooté

## Licence

Licence Publique IV : voir [LICENSE](LICENSE) ou https://www.shaftinc.fr/licence-publique-iv.html
