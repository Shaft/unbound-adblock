## [1.4] 2023-02-10
- 2023-02-10 : Meilleure gestion de la suppression des fichiers temporaires
- 2022-08-09 : Ajout de la procédure d'installation via paquet dans le README
## [1.3] 2021-12-29
- 2021-12-29 : Début du durcissement du service (ajout PrivateTmp et ProtectHome)
- 2021-12-29 : Assignation par défaut du JSON dans le répertoire `/var/lib/unbound` dans le fichier du service
- 2021-12-29 : Amélioration du test pour voir si `nbListes` est vide
- 2021-10-21 : Correction d'un problème dans le tri des listes (voir #11)
## [1.2] 2020-06-06
- 2020-06-06 : Ajout d'un test pour voir si le JSON est vide (voir #9)
- 2020-04-10 : Remplacement d'une des liste du JSON (désormais inaccessible, voir #10)
- 2019-09-28 : Refonte du README
- 2019-09-10 : Ajout d'un timer systemd (Contourne #3)
- 2019-09-07 : Remplacement des espaces dans le nom d'une liste dans le JSON par des tirets pour le nom du fichier dans lequel on stocke la liste (voir #6)
## [1.1] 2019-09-03
- 2019-09-03 : Prise en compte du retrait de domaine des listes téléchargées (voir #5)
- 2019-09-03 : Ajout de la détection du retrait d'entrées dans le JSON (voir #4)
- 2019-09-02 : Ajout du contrôle des dépendances
## [1.0] 2019-09-01
- 2019-09-01 : Commit initial et publication
